"""Skeleton for the game.
"""

black = (0,0,0)
white = [1,1,1]
from math import pi, sin, cos

from direct.showbase.ShowBase import ShowBase

#from panda3d.core import NodePath, TextNode
#from panda3d.core import PointLight, AmbientLight
from direct.gui.OnscreenText import OnscreenText
from direct.showbase.ShowBase import ShowBase
from direct.task import Task
from direct.actor.Actor import Actor
from direct.interval.IntervalGlobal import Sequence
from direct.interval.MetaInterval import Parallel
from panda3d.core import Point3
from direct.showbase.DirectObject import DirectObject
import sys
import time


def add_text(text,place,position = (-0.1, 0.09)):
    """To add textual things"""
    return OnscreenText(text=text, style=1, fg=(1, 1, 1, 1), scale=.08,
                    parent=parentu, align=TextNode.ARight,
                    pos=position, shadow=(0, 0, 0, 1))



class main_class(ShowBase):
    """The main class of the game."""

    def __init__(self):
        """Initializing everything."""

        ShowBase.__init__(self)

        base.disableMouse()

        self.win_props = base.win.getProperties()
        self.screens_X = self.win_props.getXSize()
        print(self.screens_X)

        base.setBackgroundColor(black)
        self.bg = list(base.getBackgroundColor())

        self.set_camera()
        self.cam_posi = list(self.camera.getPos())

        self.op_sound = base.loader.loadSfx("ecg.mp3")
        self.op_sound.play()
        vol = self.op_sound.getVolume()

        self.fade_vol(self.op_sound,vol)

        self.pandaActor = Actor("models/panda-model",
                                {"walk": "models/panda-walk4"})
        self.pandaActor.setScale(0.005, 0.005, 0.005)
        self.pandaActor.reparentTo(self.render)

        self.models = [self.pandaActor] # Add more as you get more models
        self.move_task = taskMgr.add(self.move, "move around")


    def fade_vol(self,cur_sound,cur_vol):
        """Fades the volume out and changes the background color"""
        k = cur_vol
        time.sleep(3)
        while k >= 0.1:
            k -= 0.1
            cur_sound.setVolume(k)
            time.sleep(0.5)
        cur_sound.stop()
        base.setBackgroundColor(white)


    def set_camera(self):
        """Setting the camera position and oreintation"""
        self.camera.setPos(1, -20, 1)
        self.camera.setHpr(2, 0, 0)


    def move_forward(self):
        """Moves the camera forward"""
        check = True # Checks distance with every model. Need a more efficient
        # way if possible
        k = self.cam_posi[1]
        self.cam_posi[1]+=1
        for i in self.models:
            if self.distance_checker(self.cam_posi, i.getPos()):
                check = check and True
            else:
                check = check and False
        if check == True:
            self.camera.setPos(tuple(self.cam_posi))
        else:
            self.cam_posi[1] = k
            self.camera.setPos(tuple(self.cam_posi))


    def move_left(self):
        """Moves the camera left"""
        check = True
        k = self.cam_posi[0]
        self.cam_posi[0]-=1
        for i in self.models:
            if self.distance_checker(self.cam_posi, i.getPos()):
                check = check and True
            else:
                check = check and False

        if check == True:
            self.camera.setPos(tuple(self.cam_posi))
        else:
            self.cam_posi[0] = k
            self.camera.setPos(tuple(self.cam_posi))


    def move_back(self):
        """Moves the camera down"""
        check = True
        k = self.cam_posi[1]
        self.cam_posi[1]-=1
        for i in self.models:
            if self.distance_checker(self.cam_posi, i.getPos()):
                check = check and True
            else:
                check = check and False

        if check == True:
            self.camera.setPos(tuple(self.cam_posi))
        else:
            self.cam_posi[1] = k
            self.camera.setPos(tuple(self.cam_posi))


    def move_right(self):
        """Moves the camera right"""
        check = True
        k = self.cam_posi[0]
        self.cam_posi[0] += 1
        for i in self.models:
            if self.distance_checker(self.cam_posi, i.getPos()):
                check = check and True
            else:
                check = check and False

        if check == True:
            self.camera.setPos(tuple(self.cam_posi))
        else:
            self.cam_posi[0] = k
            self.camera.setPos(tuple(self.cam_posi))



    def distance_checker(self, point1, point2):
        """Checks the distance between point1 and point2"""
        dist = ((point1[0]-point2[0]) ** 2 + (point1[1]-point2[1]) ** 2 + (point1[2]-point2[2]) ** 2) ** (0.5)
        if dist > 4:
            return True
        else:
            return False


    def move(self, task):
        """Check for keypress every frame"""
        self.accept('escape',sys.exit)
        self.accept('w',self.move_forward)
        self.accept('a',self.move_left)
        self.accept('s',self.move_back)
        self.accept('d',self.move_right)
        return Task.cont


"""
class classroom_scene(main_class):
    def __init__(self):

"""
# kyu = Classroom()
game = main_class()
game.run()
