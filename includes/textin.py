from direct.showbase.ShowBase import ShowBase
from panda3d.core import *
class MyApp(ShowBase):
 
    def __init__(self):
        ShowBase.__init__(self)

        font = self.loader.loadFont('models/fonts/Super_Renewables.otf')
        font.setPixelsPerUnit(60)

        text = TextNode('name')
        text.setText("Every day in every way I'm getting better and better.")
        text.setFont(font)
        text.setAlign(TextNode.ACenter)
        text.setWordwrap(15.0)
        text.setSmallCapsScale(20)
        text.setTextColor(1, 0.5, 0.5, 1)
        text.setShadow(0.05, 0.05)
        text.setShadowColor(0, 0, 0, 1)
        # text.setFrameColor(0, 0, 1, 1)
        # text.setFrameAsMargin(0.2, 0.2, 0.1, 0.1)
        # text.setCardColor(1, 1, 0.5, 1)
        # text.setCardAsMargin(0, 0, 0, 0)
        # text.setCardDecal(True)

        textNodePath = self.aspect2d.attachNewNode(text)
        textNodePath.setScale(0.2)
        
app = MyApp()
app.run()